---
dribbble: https://dribbble.com/
default: true
youtube: https://www.youtube.com/
github: https://github.com/
vimeo: https://vimeo.com/
facebook: https://www.facebook.com/
flickr: https://www.flickr.com/
bio: Writer at WorkBox Publishing, parent, aminal lover and avid coffee drinker.
name: enrico
display_name: Enrico Stano
instagram: https://www.instagram.com/
twitter: https://twitter.com/
pinterest: https://www.pinterest.com
website: https://mysite.com
image: https://pbs.twimg.com/profile_images/974603248119222272/N5PLzyan.jpg
email: me@mysite.com
linkedin: https://www.linkedin.com/
reddit: https://www.reddit.com/
---
program or be programmed - pushing social economy, open and platform cooperativis at coopdevs.org