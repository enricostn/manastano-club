---
layout: post
title: Paura del delirio di Las Vegas
date: 2020-09-22T16:51:16.540Z
hidden: false
category:
  - viaggi
author: gio
image: /uploads/img_20141125_013427.jpg
---
Mai nella vita avrei pensato di ritrovarmi in questa città demenziale.

E invece nel novembre del 2014, durante un viaggio nel Far West americano, scopriamo che è un’ottima base di partenza per i bellissimi parchi della zona (Bryce, Zion, Monument Valley, Grand Canyon): le auto si affittano a prezzi convenienti, gli hotel hanno spesso buone offerte e si trovano ristoranti di tutti i tipi (ancora mi sogno i magnifici pancakes dello storico Golden Gate). Ci sono anche shopping mall dove si possono trovare buone occasioni per scarpe o indumenti.

Per il resto Las Vegas mi sembra un luna park delirante per adulti lobotomizzati, un’oasi nel deserto del disagio umano, un girone infernale dove il Dio Denaro si diverte a mescolare cowboys, turisti, luci, suoni, musichette, cocktails di gamberetti, disperati, aria condizionata, automobili, gondole, piramidi, fontane, gladiatori, Elvis, giocatori, ubriachi e puttane.

L’unica cosa al mondo peggio di Las Vegas non può essere che Las Vegas d’estate.

Adesso che ci sono stata posso dirlo: questa città è un brufolo sul culo del mondo.