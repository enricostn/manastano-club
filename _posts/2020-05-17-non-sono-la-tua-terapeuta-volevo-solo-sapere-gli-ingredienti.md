---
layout: post
title: Volevo solo sapere gli ingredienti
date: 2020-09-22T16:24:23.868Z
hidden: false
category:
  - cibo
author: gio
image: /uploads/img_20141018_021723.jpg
---
Spesso le ricette pubblicate su Internet sono corredate da interminabili quanto fastidiosi sproloqui autobiografici.

La ricetta vera e propria è preceduta da un numero variabile di paragrafi innecessari in cui l’autrice del post (di solito femmina e logorroica) narra della salute sua o dei suoi simili, condivide aneddoti di vita vuota, elargisce pillole di noia e consigli su come ignorare lo squallore della propria esistenza bevendo una tisana.

All’inizio, ingenua, leggevo pedestremente tutto il contenuto dei post, per poi arrivare alla lista degli ingredienti infastidita e senza residua voglia di cucinare.

Nel tentativo di capire quante mele servono per la torta, mi ritrovo impantanata nelle vicende personali di qualcuno che non conosco e che, riga dopo riga, spero di non dover conoscere mai.

Possibile che sia questo l'amaro prezzo da pagare per ottenere una ricetta?

Perché non puoi scrivere come fai la crostata e basta?

Non sono la tua psicoterapeuta, mi vuoi dire per favore a che temperatura devo accendere 'sto forno?



\# culiinaria